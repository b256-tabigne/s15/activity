console.log("Hello World")

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let firstname = "Jomyrr";
	console.log("First Name: " + firstname);

	let lastname = "Tabigne";
	console.log("Last Name: " + lastname);

	let age = 24;
	console.log("Age: " + age);

	let hobbies = ["Reading Manga", "Watching Anime", "Playing Games"];
	console.log("Hobbies:")
	console.log(hobbies);

	let wa = {
	HouseNumber: "8145",
	Street: "4th Floor LeGatch Building",
	State: "Metro Manila",
	City: "Parañaque",
	}
	console.log("Work Address:")
	console.log(wa)

	console.log("My full name is: " + firstname + lastname)
	console.log("My current age is: " + age)


	
	let friends = ["Annabelle", "Chucky", "Pennywise", "Jason", "Jigsaw", "Jason Bourne"];
	console.log("My Friends are: ")
	console.log(friends)
	
	let profile = {

		username: "jomyrryanihh",
		fullName: "Jomyrr Jeff Cruz Tabigne",
		age: 24,
		isActive: false,
	}
	console.log("My Full Profile: ")
	console.log(profile);

	let colleague = "Jonathan Cruz";
	console.log("My colleague is: " + colleague);

	const lastLocation = "SM Bicutan";
	// lastLocation = "Atlantic Ocean";
	console.log("I was last found in: " + lastLocation);